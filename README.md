![](lekhanee/graphic-assets/feature_graphic.png)

# लेखनी कि-बोर्ड (Lekhanee Android) 

> **लेखनी** -- *नाम [संस्कृत]* लेख्ने साधन; कलम । 

> -- नेपाली बृहत् शब्दकोश


Lekhanee Android is a soft keyboard for Nepali languages for Android. 

This project is based on [indic-keyboard] (https://gitlab.com/smc/indic-keyboard) which in turn is based on AOSP's [LatinIME](https://android.googlesource.com/platform/packages/inputmethods/LatinIME) package.

## Support

Feel free to create an issue if you have suggestions for improvements, questions or a bug to report - [Issue Page](https://gitlab.com/lekhanee/lekhanee-android/issues). If you don't have a Gitlab account you can create an issue by sending an email to `incoming+lekhanee/lekhanee-android@gitlab.com`.


## Screenshots

![Nepali Keyboard](https://gitlab.com/lekhanee/lekhanee-android/raw/master/graphic-assets/lekhanee/screenshot1.jpeg)

## Documentation

Please see the [wiki page](https://gitlab.com/lekhanee/lekhanee-android/wikis/home).

## Instructions to Build

1. Install gradle, Android Support Repository, SDK and other usual android stuffs,
    1. Download the necessary tools from https://developer.android.com/studio/index.html and install them. Also set the necessary environment variables like `ANDROID_HOME`

2. `git clone --recursive https://gitlab.com/lekhanee/lekhanee-android.git`
4. `cd java`
5. `gradle assembleDebug` to build the package.

Please see the `indic-keyboard.md` file for additional development README.

## License

indic-keyboard is licensed under [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.html)

Code that is added in lekhanee-android is licensed under [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

All other contents are [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)

