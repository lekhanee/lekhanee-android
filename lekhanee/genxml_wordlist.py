from time import time

DESCRIPTION = {
    'locale'        : "ne_NP",
    'description'   : "Nepali (NP)",
    'date'          : round(time()),
    'version'       : "0",
}

def scale(freq, freq_max, freq_min, scale_max=222, scale_min=15):
    return freq / ((freq_max - freq_min) / (scale_max - scale_min)) + scale_min

def first_line(description_dict):
    s = '<wordlist'
    for k, v in description_dict.items():
        s += ' {0}="{1}"'.format(k, v)
    s += '>'
    return s

def genxml_wordlist(in_filename, out_filename):
    with open(out_filename, 'w') as out_file:
        out_file.write(first_line(DESCRIPTION))
        out_file.write('\n')
        with open(in_filename, 'r') as in_file:
            parsed_list = [] # PERF because holding in memory
            freq_max = 0
            freq_min = 1000
            for line in in_file:
                freq, word = [item.strip() for item in line.split(',')]
                int_freq = int(freq)
                parsed_list.append({ "word": word, "freq": int_freq })
                if int_freq > freq_max:
                    freq_max = int_freq
                if int_freq < freq_min:
                    freq_min = int_freq
            for word_dict in parsed_list:
                word = word_dict["word"]
                freq = scale(word_dict["freq"], freq_max, freq_min)
                out_line = ' <w f="{0}" flags="">{1}</w>'.format(freq, word)
                print out_line
                out_file.write(out_line)
                out_file.write('\n')
        out_file.write("</wordlist>")

if __name__ == "__main__":
    genxml_wordlist('no_freq-ne.txt', 'ne_np_wordlist.xml')
