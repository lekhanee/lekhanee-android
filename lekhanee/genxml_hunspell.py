from time import time

DEFAULT_FREQ = 100

DESCRIPTION = {
    'locale'        : "ne_NP",
    'description'   : "Nepali (NP)",
    'date'          : round(time()),
    'version'       : "0",
}

def first_line(description_dict):
    s = '<wordlist'
    for k, v in description_dict.items():
        s += ' {0}="{1}"'.format(k, v)
    s += '>\n'
    return s

def genxml_hunspell(in_filename, out_filename):
    with open(out_filename, "w") as out_file:
        out_file.write(first_line(DESCRIPTION))
        with open(in_filename, "r") as hun_dic:
            for line in hun_dic:
                word = line.split("/")[0].strip()
                out_file.write(' <w f="{0}" flags="">{1}</w>\n'.format(DEFAULT_FREQ, word))
        out_file.write("</wordlist>")

if __name__ == "__main__":
    genxml_hunspell("ne_NP.dic_some", "ne_NP_hunspell.xml")
